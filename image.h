#ifndef IMAGE_H
#define IMAGE_H

char *read_image(char *filename, unsigned *width, unsigned *height);
void free_image(char *raw_bits);

#endif //IMAGE_H
