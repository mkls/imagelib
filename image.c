#include "image.h"

#include <stdlib.h>
#include <stdio.h>
#include <FreeImage.h>

char *read_image(char *filename, unsigned *width, unsigned *height){
    FreeImage_Initialise(FALSE);
    FREE_IMAGE_FORMAT image_format = FreeImage_GetFileType(filename, 0);

    if (image_format == FIF_UNKNOWN) {
        fprintf(stderr, "Uknown image format\n");
        return NULL;
    }

    FIBITMAP *image = FreeImage_Load(image_format, filename, 0);
    if (image == NULL) {
        fprintf(stderr, "Failed to load image %s\n", filename);
        return NULL;
    }

    FIBITMAP *temp = FreeImage_ConvertTo32Bits(image);
    if (temp == NULL) {
        fprintf(stderr, "Failed to convert image %s to 32bit format\n", filename);
        FreeImage_Unload(image);
        return NULL;
    }

    FreeImage_Unload(image);

    *width = FreeImage_GetWidth(temp);
    *height = FreeImage_GetHeight(temp);
    int scan_width = FreeImage_GetPitch(temp);

    BYTE* raw_bits = (BYTE*)malloc((*height) * scan_width);
    FreeImage_ConvertToRawBits(raw_bits, temp, scan_width, 32,
            FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, FALSE);
    FreeImage_Unload(temp);

    return (char*)raw_bits;
    //return (char*)FreeImage_GetBits(temp);
}

void free_image(char *raw_bits) {
    free(raw_bits);
}
